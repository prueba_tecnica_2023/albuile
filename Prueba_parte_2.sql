-- Seleccionar todos los clientes.
SELECT *
FROM [prueba].[dbo].[CLIENTE];

-- Seleccionar los clientes que pertenezcan a la regi�n �Centro�.
SELECT *
FROM [prueba].[dbo].[CLIENTE]
WHERE Region = 'Centro';

-- Seleccionar los clientes que tengan m�s de 3 cuentas abiertas en estado Activo.
SELECT [Cedula]
FROM [prueba].[dbo].[CLIENTE]
INNER JOIN [prueba].[dbo].[CUENTAS]
ON CLIENTE.Cedula = CUENTAS.Cedula_cliente
WHERE CUENTAS.Estado = '1'
GROUP BY [Cedula]
HAVING COUNT(DISTINCT [Num_cuenta]) >= 3;

-- Seleccionar solamente el nombre de aquellos clientes que tienen clave din�mica.
SELECT [Nombre]
FROM [prueba].[dbo].[CLIENTE]
INNER JOIN [prueba].[dbo].[CLAVE_DINAMICA]
ON CLIENTE.Cedula = CLAVE_DINAMICA.Cedula_cliente;

-- Seleccionar los clientes que no tienen clave din�mica.
SELECT CLIENTE.*
FROM [prueba].[dbo].[CLIENTE]
LEFT JOIN [prueba].[dbo].[CLAVE_DINAMICA]
ON CLIENTE.Cedula = CLAVE_DINAMICA.Cedula_cliente
WHERE [Cedula_cliente] IS NULL

-- Mostrar el saldo total de todas las cuentas agrupado por la regi�n del cliente.
SELECT [Region]
      ,SUM([Saldo]) AS Saldo_total
FROM [prueba].[dbo].[CLIENTE]
RIGHT JOIN [prueba].[dbo].[CUENTAS]
ON CLIENTE.Cedula = CUENTAS.Cedula_cliente
GROUP BY [Region];

-- Seleccionar el saldo total de las cuentas activas, abiertas en el mes de mayo de 2018, cuyos clientes tengan clave din�mica.
SELECT SUM([Saldo]) AS Saldo_total
FROM [prueba].[dbo].[CLIENTE]
LEFT JOIN [prueba].[dbo].[CUENTAS] ON CLIENTE.Cedula = CUENTAS.Cedula_cliente 
LEFT JOIN [prueba].[dbo].[CLAVE_DINAMICA] ON CLIENTE.Cedula = CLAVE_DINAMICA.Cedula_cliente
WHERE [Fecha_apertura] LIKE '%201805%' AND [Estado]= '1'
GROUP BY [Cedula]