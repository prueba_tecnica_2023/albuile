----------------------------------------------------- SQL parte 1 -----------------------------------------------------------

------------------	Punto 1  ----------------------------------------

SELECT [radicado]
		,[num_documento]
		,[cod_segm_tasa]
		,[cod_subsegm_tasa]
		,[cal_interna_tasa]
		,[id_producto]
		,[tipo_id_producto]
		,[valor_inicial]
		,[fecha_desembolso]
		,[plazo]
		,[cod_periodicidad]
		,[periodicidad]
		,[saldo_deuda]
		,[modalidad]
		,[tipo_plazo]
		,[cod_segmento]
		,[segmento]
		,[cod_subsegmento]
		,[calificacion_riesgos]
		,[tasa_cartera]
		,[tasa_operacion_especifica]
		,[tasa_hipotecario]
		,[tasa_leasing]
		,[tasa_sufi]
		,[tasa_factoring]
		,[tasa_tarjeta]
		,(CASE
			WHEN id_producto LIKE '%cartera%' THEN tasa_cartera
			WHEN id_producto LIKE '%operacion%' THEN tasa_operacion_especifica
			WHEN id_producto LIKE '%hipotecario%' THEN tasa_hipotecario
			WHEN id_producto LIKE '%leasing%' THEN tasa_leasing
			WHEN id_producto LIKE '%sufi%' THEN tasa_sufi
			WHEN id_producto LIKE '%factoring%' THEN tasa_factoring
			WHEN id_producto LIKE '%tarjeta%' THEN tasa_tarjeta
		END) as [tasa_producto]
		INTO [prueba].[dbo].[Obligaciones_clientes_tasas]
FROM [prueba].[dbo].[Obligaciones_clientes]
LEFT JOIN [prueba].[dbo].[tasas_productos]
ON Obligaciones_clientes.cod_segm_tasa = tasas_productos.cod_segmento
WHERE cal_interna_tasa = calificacion_riesgos AND cod_subsegm_tasa= cod_subsegmento;

------------------	Punto 2  ----------------------------------------

SELECT *
	  ,((((POWER((1+[tasa_producto]),(1/(12/CAST([cod_periodicidad] AS FLOAT)))))-1)*(12/CAST([cod_periodicidad] AS FLOAT)))/(12/CAST([cod_periodicidad] AS FLOAT))) as tasa_efectiva
	  ,(((((POWER((1+[tasa_producto]),(1/(12/CAST([cod_periodicidad] AS FLOAT)))))-1)*(12/CAST([cod_periodicidad] AS FLOAT)))/(12/CAST([cod_periodicidad] AS FLOAT)))* [valor_inicial]) as valor_inicial_te
	  

INTO [prueba].[dbo].[Tasas_obligaciones]
FROM [prueba].[dbo].[Obligaciones_clientes_tasas];

------------------	Punto 3  ----------------------------------------

SELECT [num_documento], SUM(saldo_deuda) AS total_deuda
INTO [prueba].[dbo].[Deuda_clientes]
FROM [prueba].[dbo].[Tasas_obligaciones]
GROUP BY [num_documento]

--------------------------------------------Base final-----------------------------------------------------------		
	SELECT	 [radicado]
		,[Tasas_obligaciones].[num_documento]
		,[cod_segm_tasa]
		,[cod_subsegm_tasa]
		,[cal_interna_tasa]
		,[id_producto]
		,[tipo_id_producto]
		,[valor_inicial]
		,[fecha_desembolso]
		,[plazo]
		,[cod_periodicidad]
		,[periodicidad]
		,[saldo_deuda]
		,[modalidad]
		,[tipo_plazo]
		,[cod_segmento]
		,[segmento]
		,[cod_subsegmento]
		,[calificacion_riesgos]
		,[tasa_producto]
		,[tasa_efectiva]
		,[valor_inicial_te]
		,[total_deuda]
	INTO [prueba].[dbo].[Base_clientes_final]
	FROM [prueba].[dbo].[Tasas_obligaciones]
	LEFT JOIN [prueba].[dbo].[Deuda_clientes]
	ON [Tasas_obligaciones].[num_documento]= [Deuda_clientes].[num_documento]
	ORDER BY [num_documento]